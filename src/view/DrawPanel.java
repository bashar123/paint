package view;

import controller.Controller;
import model.shapes.Shape;
import observer.Observer;
import observer.Subject;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;


public class DrawPanel extends JPanel implements Observer{

    private Controller controller;
    private ArrayList<Shape> shapes;
    private Shape selectDot, tempShape;

    public DrawPanel(Controller controller){
        this.controller = controller;
        this.addMouseListener(controller);
        this.addMouseMotionListener(controller);
        shapes = new ArrayList<>();

    }

    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        for(int i=0;i<shapes.size();i++){
            shapes.get(i).draw(g);
        }
        if(selectDot!=null)
            selectDot.draw(g);
        if(tempShape!=null)
            tempShape.draw(g);
    }

    @Override
    public void update(ArrayList<Shape> shapes, Shape selectDot, Shape tempShape) {
        this.shapes = shapes;
        this.selectDot = selectDot;
        this.tempShape = tempShape;
        this.repaint();
    }

}
