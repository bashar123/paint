package view;

import controller.Controller;

import javax.swing.*;
import java.awt.*;


public class Panel2  extends JPanel {
	private JButton  normalStrokeButton, thickStrokeButton, dashStrokeButton, deleteButton;
	private Controller controller;

	public Panel2(Controller controller){
		this.controller = controller;
		setLayout(new GridLayout(2, 2, 0, 0));

		normalStrokeButton = new JButton();
		normalStrokeButton.setIcon(new ImageIcon("./src/images/normal.png"));
		normalStrokeButton.setBackground(Color.WHITE);
		normalStrokeButton.setActionCommand("Normal");

        thickStrokeButton = new JButton();
		thickStrokeButton.setIcon(new ImageIcon("./src/images/thick.png"));
		thickStrokeButton.setBackground(Color.WHITE);
		thickStrokeButton.setActionCommand("Thick");

        dashStrokeButton = new JButton();
		dashStrokeButton.setIcon(new ImageIcon("./src/images/dashed.png"));
		dashStrokeButton.setBackground(Color.WHITE);
		dashStrokeButton.setActionCommand("Dashed");

        deleteButton = new JButton();
		deleteButton.setIcon(new ImageIcon("./src/images/delete.png"));
		deleteButton.setBackground(Color.WHITE);
		deleteButton.setActionCommand("Delete");

        addComponents();
        addButtonListeners();
	}

	private void addComponents(){

		this.add(normalStrokeButton);
		this.add(thickStrokeButton);
		this.add(dashStrokeButton);
		this.add(deleteButton);
	}

	private void addButtonListeners(){

		normalStrokeButton.addActionListener(controller);
		thickStrokeButton.addActionListener(controller);
		dashStrokeButton.addActionListener(controller);
		deleteButton.addActionListener(controller);
	}

}