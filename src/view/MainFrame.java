package view;

import controller.Controller;
import model.ModelFacade;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;


public class MainFrame extends JFrame {

    private Controller controller;
    private ModelFacade model;

    private JPanel contentPane;
    private DrawPanel drawPanel;
    private ToolPanel toolPanel;

    public MainFrame(){
        model = new ModelFacade();
        controller = new Controller(model);


        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout(0, 0));

        drawPanel = new DrawPanel(controller);
        toolPanel = new ToolPanel(controller);

        model.register(drawPanel); //register observer

        contentPane.add(toolPanel, BorderLayout.WEST);
        contentPane.add(drawPanel, BorderLayout.CENTER);

    }

}
