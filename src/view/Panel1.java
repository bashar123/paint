package view;

import controller.Controller;

import javax.swing.*;
import java.awt.*;


public class Panel1 extends JPanel {

	private JButton circleButton, lineButton, rectButton;
	private JCheckBox filledBox;
	private Controller controller;

	public Panel1(Controller controller) {
		setLayout(new GridLayout(2, 2, 0, 0));
		this.controller = controller;

		rectButton = new JButton();
		rectButton.setIcon(new ImageIcon("./src/images/Untitled-1.png"));
		rectButton.setBackground(Color.WHITE);
		rectButton.setActionCommand("Rectangle");

        circleButton = new JButton();
		circleButton.setIcon(new ImageIcon("./src/images/circle.png"));
		circleButton.setBackground(Color.WHITE);
		circleButton.setActionCommand("Circle");

        lineButton = new JButton();
		lineButton.setIcon(new ImageIcon("./src/images/line.png"));
		lineButton.setBackground(Color.WHITE);
		lineButton.setActionCommand("Line");

        filledBox = new JCheckBox("Filled");
		filledBox.setHorizontalAlignment(SwingConstants.CENTER);
		filledBox.setBackground(Color.WHITE);

        addComponents();
        addButtonListeners();
	}

	private void addComponents(){
		this.add(rectButton);
		this.add(circleButton);
		this.add(lineButton);
		this.add(filledBox);
	}

	private void addButtonListeners(){
		rectButton.addActionListener(controller);
		circleButton.addActionListener(controller);
		lineButton.addActionListener(controller);
		filledBox.addActionListener(controller);
	}
}
