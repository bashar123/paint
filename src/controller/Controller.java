package controller;

import model.*;
import model.shapes.*;
import model.shapes.Rectangle;
import model.shapes.Shape;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


public class Controller implements ActionListener, MouseListener, MouseMotionListener{

    private ModelFacade model;
    private String pressedButtonName,currentStrokeStyle;
    private boolean isFilled;

    private Shape selectedShape;
    private Point startPos;

    public Controller(ModelFacade model){
        this.model = model;
        this.currentStrokeStyle = "Normal" ;

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("You pressed a button: " + e.getActionCommand());

        switch (e.getActionCommand()){
            case "Filled":
                isFilled = !isFilled;
                if(selectedShape instanceof FillableShape){
                    model.setFilled((FillableShape) selectedShape, isFilled);
                }
                break;
            case "Normal":
            case "Thick":
            case "Dashed": {
                currentStrokeStyle = e.getActionCommand();
                model.setStrokeStyle(selectedShape, currentStrokeStyle);
                break;
            }
            case "Save":
                model.saveToFile();
                break;
            case "Load":
                model.loadFromFile();
                break;
            case "Delete":
                model.deleteShape(selectedShape);
                break;
            case "Undo":
                model.undo();
                break;
            case "Redo":
                model.redo();
                break;
            case "Color":
                Color color = JColorChooser.showDialog(null, "Choose color", Color.black);
                model.changeColor(selectedShape, color);
                break;
            default:
                pressedButtonName = e.getActionCommand();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        startPos = e.getPoint();

        model.drawShape(pressedButtonName, startPos, e.getPoint(), currentStrokeStyle, isFilled);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        model.drawShape(pressedButtonName, startPos, e.getPoint(), currentStrokeStyle, isFilled);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if(pressedButtonName != null){
            model.addShape(pressedButtonName, startPos, e.getPoint(), currentStrokeStyle, isFilled);
            startPos = null;
            pressedButtonName = null;
        }
        else {  //select Shape
            selectedShape = model.getShapeByCoordinates(e.getPoint());
            model.markShape(selectedShape);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {}
    @Override
    public void mouseEntered(MouseEvent e) {}
    @Override
    public void mouseExited(MouseEvent e) {}
    @Override
    public void mouseMoved(MouseEvent e) {}

}
