package model.redo;

import model.ModelFacade;
import model.undo.UndoDeleteCommand;


public class RedoDeleteCommand implements RedoCommand {
	private ModelFacade model;

	public RedoDeleteCommand(ModelFacade model ){
		this.model = model;
	}

	@Override
	public void exceute() {
		if(model.shapes.size()!=0){

			model.deletedShapes.add(model.shapes.get(model.shapes.size() - 1));
			model.shapes.remove(model.shapes.size() - 1);

			UndoDeleteCommand undoDeleteCommand = new UndoDeleteCommand(model);
			model.undoInvoker.addCommandToStack(undoDeleteCommand);

		}
	}
}
