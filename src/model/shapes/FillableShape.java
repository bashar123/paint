package model.shapes;


public abstract class FillableShape extends Shape {

    private int width, height;
    private boolean filled;

    public FillableShape(int x, int y, int x2, int y2, String currentStroke, boolean filled) {
        super(x, y, x2, y2, currentStroke);
        this.filled = filled;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public boolean isFilled(){
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public void toggleFilled(){
        filled = !filled;
    }


}
