package model.shapes;

import java.util.ArrayList;

//PROTOTYPE PATTERN
public class ShapeFactory {

	private ArrayList<Shape> sampleShapes;

	public ShapeFactory(){
		sampleShapes = new ArrayList<>();

		//skapar en cache av prototypes
		sampleShapes.add(new Circle(0,0,0,0,"Normal",false));
		sampleShapes.add(new Rectangle(0,0,0,0,"Normal",false));
		sampleShapes.add(new Line(0,0,0,0,"Normal"));
	}

	
	public ArrayList<Shape> getSampleShapes(){
		return sampleShapes;
	}

	
	public Shape getClone(Shape sampleShape){
		return sampleShape.createCopy();
	}

}
