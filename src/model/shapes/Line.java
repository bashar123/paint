package model.shapes;

import java.awt.*;
import java.awt.geom.Line2D;


public class Line extends Shape {

    private Line2D line;

    public Line(int x1, int y1, int x2, int y2, String currentStroke){
        super(x1, y1, x2, y2, currentStroke);
    }

    @Override
    protected void drawShape(Graphics2D g) {
        line = new Line2D.Double(super.getX(), super.getY(), super.getX2(), super.getY2());
        g.draw(line);
    }

    @Override
    public boolean isHit(double x, double y){
        if(line!=null){
            return line.intersects( x, y, 2, 2);
        }
        return false;
    }

    @Override
    public Shape createCopy() {
        System.out.println("Line is copping");
        Line line = null;
        try {
            line = (Line) super.clone();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return line;
    }
}
