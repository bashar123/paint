package model.shapes;



import java.awt.*;
import java.io.Serializable;

public abstract class Shape implements Serializable, Cloneable{

    private int x, y, x2, y2;
    private Color color;
    private String currentStroke;

    public Shape(int x, int y, int x2, int y2, String currentStroke){
        this.x = x;
        this.y = y;
        this.x2 = x2;
        this.y2 = y2;

        this.currentStroke = currentStroke;
        this.color = Color.BLACK;
    }

    public final void draw(Graphics g){
        Graphics2D g2 = (Graphics2D) g;

        g2.setColor(color);
        switch (currentStroke){
            case "Normal":
                g2.setStroke(new BasicStroke(1));
                break;
            case "Thick":
                g2.setStroke(new BasicStroke(10));
                break;
            case "Dashed":
                float dash[] = {10.0f};
                g2.setStroke(new BasicStroke(2.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f));
                break;
        }

        drawShape(g2);
    }

    public void setColor(Color color){
        this.color = color;
    }

    protected abstract void drawShape(Graphics2D g);

    public abstract boolean isHit(double x, double y);

    public abstract Shape createCopy();

    public int getX2() {
        return x2;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public int getY2() {
        return y2;
    }

    public void setY2(int y2) {
        this.y2 = y2;
    }

    public int getX(){
        return x;
    }

    public int getY(){
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setCurrentStroke(String currentStroke){
        this.currentStroke = currentStroke;
    }

    public String getCurrentStroke(){
        return currentStroke;
    }
}

