package model.shapes;

import java.awt.*;
import java.awt.geom.Rectangle2D;

public class Rectangle extends FillableShape {


    private Rectangle2D rectangle;

    public Rectangle(int x, int y, int x2, int y2, String currentStroke, boolean filled){
        super(x, y, x2, y2, currentStroke, filled);
    }


    @Override
    protected void drawShape(Graphics2D g) {

        rectangle = new Rectangle2D.Double(super.getX(), super.getY(), super.getWidth(), super.getHeight());

        if(super.isFilled()){
            g.fill(rectangle);
        } else {
            g.draw(rectangle);
        }
    }

    @Override
    public boolean isHit(double x, double y) {
        if(rectangle!=null){
            return rectangle.contains(x,y);
        }
        return false;
    }

    @Override
    public Shape createCopy() {
        System.out.println("Rectangle is copping");
        Rectangle rec = null;
        try {
            rec = (Rectangle) clone();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rec;
    }

}
