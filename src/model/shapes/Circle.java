package model.shapes;

import java.awt.*;
import java.awt.geom.Ellipse2D;

public class Circle extends FillableShape {

    private Ellipse2D ellipse;

    public Circle(int x, int y, int x2, int y2, String currentStroke, boolean filled){
        super(x, y, x2, y2, currentStroke, filled);
    }


    @Override
    protected void drawShape(Graphics2D g) {
        ellipse = new Ellipse2D.Double(super.getX(), super.getY(), super.getWidth(), super.getHeight());

        if(super.isFilled()){
            g.fill(ellipse);
        } else {
            g.draw(ellipse);
        }
    }

    @Override
    public boolean isHit(double x, double y) {
        if(ellipse!=null){
            return ellipse.contains(x,y);
        }
        return false;
    }

    @Override
    public Shape createCopy() {
        System.out.println("Circle is copping");
        Circle circle = null;
        try {
            circle = (Circle) super.clone();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return circle;
    }
}
