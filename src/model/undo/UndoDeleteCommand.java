package model.undo;

import model.ModelFacade;
import model.redo.RedoDeleteCommand;


public class UndoDeleteCommand implements UndoCommand {
	private ModelFacade model;

	public UndoDeleteCommand(ModelFacade model ){
		this.model = model;
	}

	@Override
	public void exceute() {
		if(model.deletedShapes.size()!=0){
			model.shapes.add(model.deletedShapes.get(model.deletedShapes.size() - 1));
			model.deletedShapes.remove(model.deletedShapes.size() - 1);

			RedoDeleteCommand redoDeleteCommand = new RedoDeleteCommand(model);
			model.redoInvoker.addCommandToStack(redoDeleteCommand);
		}
	}
}
