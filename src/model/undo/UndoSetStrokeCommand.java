package model.undo;

import model.redo.RedoInvoker;
import model.redo.RedoSetFilledCommand;
import model.redo.RedoSetStrokeCommand;
import model.shapes.Shape;

public class UndoSetStrokeCommand implements UndoCommand {
	private String stroke;
	private Shape shape;
	RedoInvoker redoInvoker;
	UndoInvoker undoInvoker;

	public UndoSetStrokeCommand(Shape shape, String stroke, RedoInvoker redoInvoker, UndoInvoker undoInvoker) {
		this.shape = shape;
		this.stroke = stroke;
		this.redoInvoker = redoInvoker;
		this.undoInvoker = undoInvoker;
	}


	@Override
	public void exceute() {

		String currentStroke = shape.getCurrentStroke();

		shape.setCurrentStroke(stroke);

		RedoSetStrokeCommand redoSetStrokeCommand = new RedoSetStrokeCommand(shape, currentStroke, redoInvoker, undoInvoker);
		redoInvoker.addCommandToStack(redoSetStrokeCommand);
	}
}