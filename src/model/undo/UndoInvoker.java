package model.undo;

import java.util.Stack;

public class UndoInvoker {
	Stack<UndoCommand> commands;

	public UndoInvoker(){
		this.commands = new Stack<>();
	}

	public void addCommandToStack(UndoCommand command){
		commands.push(command);
	}

	public void clearStack(){
		commands.clear();
	}

	public void executeCommand(){
		if(!commands.empty()){
			UndoCommand command = commands.pop();
			command.exceute();
		}
	}

}
