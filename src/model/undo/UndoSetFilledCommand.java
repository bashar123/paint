package model.undo;

import model.redo.RedoInvoker;
import model.redo.RedoSetFilledCommand;
import model.shapes.FillableShape;
import model.shapes.Shape;


public class UndoSetFilledCommand implements UndoCommand {
	private boolean filled;
	private Shape shape;
	RedoInvoker redoInvoker;
	UndoInvoker undoInvoker;

	public UndoSetFilledCommand(Shape shape, boolean filled, RedoInvoker redoInvoker, UndoInvoker undoInvoker) {
		this.shape = shape;
		this.filled = filled;
		this.redoInvoker = redoInvoker;
		this.undoInvoker = undoInvoker;
	}


	@Override
	public void exceute() {
		((FillableShape) shape).setFilled(filled);

		RedoSetFilledCommand redoSetFilledCommand = new RedoSetFilledCommand(shape, !filled, redoInvoker, undoInvoker);
		redoInvoker.addCommandToStack(redoSetFilledCommand);
	}
}
